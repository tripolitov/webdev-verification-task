![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)

PREREQUIREMENTS

    -NodeJS v6.10.x or above (https://nodejs.org) must be installed and 'npm' command must be available in the system	

SETUP

    -Clone repository to some folder
    -Navigate to the folder and run 'npm i' (without quotes) to install required node modules
    -Run 'nmp run serve'
    -This will build the project, run development server and open index.html in your default browser.
    -Once the server is up & running you can change project files, browser gets updated automatically.

REQUIREMENTS

    Given a sketch file with design of our site you're expected to create HTML+CSS markup out of it.
    The markup must be ADOPTIVE (e.g. there're versions for desktop & mobile) and RESPONSIVE.
    There also should be minimum JS code to show/hide navigation menu on mobile version.

    You can change both project structure and build scripts the way you consider appropriate.
    You're also allowed to use any 3rd party libraries (e.g. jquery, twitter bootstrap).

ASSESSMENT CRITERIA (WHAT WE EXPECT TO SEE)

    1. Markup should perfectly match design on IE 10 (and higher), FF 50 (and higher) + mobile , Chrome 55 (and higher) + mobile, IOS Safary 9 (and higher) + mobile.
    2. Clear CSS code - a person with minimal knowledge of CSS/HTML must be able to understand and extend it
	3. Images must be displayed properly (non-blured) on high-resolution (Retina) displays
	



